import 'package:flutter/material.dart';

class Formulario {
  final String nome;
  final String email;
  final int idade;

  Formulario(
      this.nome,
      this.email,
      this.idade,
      );

  @override
  String toString() {
    return 'Formulario {Nome: $nome, Email: $email, Idade: $idade}';
  }
}

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  final TextEditingController _controladorNome = TextEditingController();
  final TextEditingController _controladorEmail = TextEditingController();
  final TextEditingController _controladorIdade = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Formulario'),
        ),
        body: Column(
          children: <Widget>[

            TextField(
              controller: _controladorNome,
              decoration: InputDecoration(labelText: 'Nome'),
            ),

            Padding(
              padding: const EdgeInsets.only(top: 16.0),
              child: TextField(
                controller: _controladorEmail,
                decoration: InputDecoration(labelText: 'Email'),
              ),
            ),

            Padding(
              padding: const EdgeInsets.only(top: 16.0),
              child: TextField(
                controller: _controladorIdade,
                decoration: InputDecoration(labelText: 'Idade'),
                keyboardType: TextInputType.number,
              ),
            ),

//            Padding(
//              padding: const EdgeInsets.only(top: 16.0),
//              child: RaisedButton( ),
//            ),

            RaisedButton(
              child: Text('Cadastrar'),

              onPressed: () {
                final String nome = _controladorNome.text;
                final String email = _controladorEmail.text;
                final int idade = int.tryParse(_controladorIdade.text);

                final Formulario form = Formulario(nome, email , idade);
                print(form);
              },
            )
          ],
        ),
      ),
    );
  }

}

